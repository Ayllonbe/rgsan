#include <Rcpp.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm> 
#include <set>
#include <iterator>
#include <cmath>
#include <map>
#include <deque> 


using namespace Rcpp;
using namespace std;


namespace ontologyF{

class Link
{
public:
  Link();
  vector<string> parents;
  vector<string> childrens;
};

Link::Link(){
  this->parents=vector<string>();
  this->childrens=vector<string>();
  
}


/*
* ANNOTATION CLASS
*/

class Annotation {
public:
  Annotation();
  set<string> getGenome();
  void setGenome(set<string> setGenes);
  set<string> getGeneSet();
  void setGeneSet(set<string> vecGenes);
  void setOneGeneInGenome(string setGenes);
  void setOneGeneInGenSet(string gene);
  void clearGeneSet();
private:
  set<string> genome;
  set<string> geneSet;
};

Annotation::Annotation(){
  
}
set<string> Annotation::getGenome(){
  return genome;
}
set<string> Annotation::getGeneSet(){
  return geneSet;
}
void Annotation::setOneGeneInGenome(string g){
  genome.insert(g);
}
void Annotation::setOneGeneInGenSet(string g){
  geneSet.insert(g);
}

void Annotation::clearGeneSet(){
  geneSet.clear();
}

void Annotation::setGeneSet(set<string> vecGenes){
  geneSet.insert(vecGenes.begin(),vecGenes.end());
  
}

void Annotation::setGenome(set<string> vecGenes){
  genome.insert(vecGenes.begin(),vecGenes.end());
  
}

/*
* TERM CLASS
*/

class Term
{
public:
  Term();
  //long double g_scores;
  //long double f_scores = 0;
  // Term *next;
  // vector<Edge> adjacencies;
  Annotation annotation;
  vector<string> combinedTerm;
  vector<long double> alphaBetaMazandu;
  void setKeyValue(string key, string val);
  void print();
  bool isObsolete();
  string getId();
  void  setId(string id);
  vector<string> getParent();
  void setChildren(string child);
  vector<string> getChildren();
  vector<string> getPOFParent();
  void setPOFChildren(string child);
  vector<string> getPOFChildren();
  string getRegulateTerm();
  string getTop();
  string getTopID();
  string getName();
  void set_Hscore(long double d);
  long double getDepth();
  long double getIC(int index);
  void setIC(long double ic);
  void setExternalIC(long double ic);
  void setAggregateIC(long double aic);
  long double getExternalIC();
  long double getAggregateIC();
  long double getSValueIC();
private:
  string id;
  string name;
  string top;
  Link is_a;
  Link part_of;
  string reg;
  string positive_reg;
  string negative_reg;
  bool obso;
  long double h_score; 
  vector<long double> ic;
  long double externalIC;
  long double sValueIC;
  long double aggregateIC;
  
  
};

void Term::setId(string i){
  id = i;
}
void Term::setIC(long double i){
  this->ic.push_back(i);
}
long double Term::getIC(int i){
  return ic.at(i);
}
string Term::getName(){
  return name;
}
void Term::setChildren(string child){
  this->is_a.childrens.push_back(child);
}
void Term::setPOFChildren(string child){
  this->part_of.childrens.push_back(child);
}
string Term::getRegulateTerm(){
  if(reg.size()>0){
    return reg;
  }else if(negative_reg.size()>0){
    return negative_reg;
  }else if(positive_reg.size()>0){
    return positive_reg;
  }else{
    return id;
  }
}

void Term::setExternalIC(long double ic){
  externalIC=ic;
  sValueIC= 1./(1. + exp(-1./ic));
}
void Term::setAggregateIC(long double aic){
  aggregateIC=aic;
}
long double Term::getExternalIC(){
  return externalIC;
}
long double Term::getAggregateIC(){
  return aggregateIC;
}

long double Term::getSValueIC(){
  return sValueIC;
}



string Term::getTop(){
  
  
  if(top.compare("biological_process") == 0){
    return "BP";
  }else if(top.compare("molecular_function") == 0){
    return "MF";
  }else{
    return "CC";
  }
}

string Term::getTopID(){
  
  
  if(top.compare("biological_process") == 0){
    return "GO:0008150";
  }else if(top.compare("molecular_function") == 0){
    return "GO:0003674";
  }else{
    return "GO:0005575";
  }
}


void Term::set_Hscore(long double d){
  this->h_score=d;
}
long double Term::getDepth(){
  return h_score;
}
vector<string> Term::getParent(){
  return is_a.parents;
}
vector<string> Term::getChildren(){
  return is_a.childrens;
}
vector<string> Term::getPOFParent(){
  return part_of.parents;
}
vector<string> Term::getPOFChildren(){
  return part_of.childrens;
}

string Term::getId(){
  return id;
}

bool Term::isObsolete(){
  return obso;
}

Term::Term()
{
  is_a = Link();
  part_of = Link();
  alphaBetaMazandu = vector<long double>(2);
  obso = false;
}


inline void Term::setKeyValue(string key, string val)
{
  if(key=="id"){
    id = val;
  }else if(key=="name"){
    name = val;
  }else if(key=="is_a"){
    string delimiter = " ! ";
    string rel = val.substr(0, val.find(delimiter));
    is_a.parents.push_back(rel);
  }else if(key=="relationship"){
    string delimiter = " ! ";
    string rel = val.substr(0, val.find(delimiter));
    val =  rel.substr( rel.find(" ")+1,rel.size());
    rel = rel.substr(0,rel.find(" "));
    if(rel=="part_of"){
      part_of.parents.push_back(val);
    }else if(rel=="regulates"){
      reg=val;
    }else if(rel=="positively_regulates"){
      positive_reg=val;
    }else if(rel=="negatively_regulates"){
      negative_reg = val;
    }
  }else if(key=="namespace"){
    top = val;
  }
  else if(key=="is_obsolete"){
    if(val=="true"){
      obso=true;
    }
  }
}

/*
* EDGE CLASS
*/
/*
class Edges
{
public: 
const long double cost;
const Term target;
const Term initial;
Edges(Term initialNode,Term targetNode, long double costVal);
void Cost(long double c);
Edges();
};

Edges::Edges(Term initialNode,Term targetNode, long double costVal):
target(targetNode),
cost(costVal),
initial(initialNode)
{
}

Edges::Edges() 
cost (0.),
initial(Term()),
target(Term())
{}
}
void Edges::Cost(long double c){
cost = c;
}
*/
/*
* ONTOINFO CLASS
*/

class OntoInfo
{
public:
  OntoInfo();
  void setMaxDepth(long double depth);
  long double getMaxDepth();
  void setMaxIC(long double ic);
  long double getMaxIC(int author);
  void setmaxDistance(long double dist);
  long double getMaxDistance();
  void setMaxExternalIC(double ic);
  long double getMaxExternalIC();
private:
  long double maxDepth;
  vector<long double> maxIC;
  long double maxExternalIC;
  long double maxDistance;
  
};
OntoInfo::OntoInfo(){
  
}
void OntoInfo::setMaxDepth(long double depth){
  this->maxDepth=depth;
}
void OntoInfo::setmaxDistance(long double dist){
  this->maxDistance=dist;
}
void OntoInfo::setMaxIC(long double ic){
  this->maxIC.push_back(ic);
}

void OntoInfo::setMaxExternalIC(double ic){
  this->maxExternalIC=ic;
}

long double OntoInfo::getMaxDepth(){
  return maxDepth;
}
long double OntoInfo::getMaxIC(int i){
  return maxIC.at(i);
}
long double OntoInfo::getMaxExternalIC(){
  return maxExternalIC;
}
long double OntoInfo::getMaxDistance() {
  return maxDistance;
}


/*
* GRAPH CLASS
*/


class Graph
{
public :
  Graph();
  double fixedDec;
  Graph( map<string,Term> str_term,map<string,OntoInfo> topTerms);
  set<string> getAllGenes(string t);
  vector<long double>  goUniverseIC(string t,string top);
  void computeGOUniverseIC(string sub);
  map<string,Term> str_term;
  //map<string,Annotation> str_annot;
  map<string,OntoInfo> topTerms;
  void setChildrens();
  set<string> getAncestors(string term);
  string getRegulateTerm(string term);
  set<string> getDescendants(string term);
  set<string> getDescendantsLeaves(string term);
  void computeInfoTerm();
  void externalIC();
  void setIC_incomplete(double ic);
  double getICIncomplete();
  //this operator enables implicit Rcpp::wrapoperatorSEXP() ;
private:
  double ic_incomplete;
  void getAnc(string term, vector<string>& anc);
  void getDesc(string term, vector<string>& dsc);
  void getDescLeaves(string term, vector<string>& dsc);
  
};

Graph::Graph( map<string,Term> strTerm,map<string,OntoInfo> top_Terms){
  fixedDec = 1000;
  str_term = strTerm;
  topTerms = top_Terms;
  
}

Graph::Graph(){
  fixedDec = 1000;
}

void Graph::setIC_incomplete(double ic){
  this->ic_incomplete=ic;
}
double Graph::getICIncomplete(){
  return this->ic_incomplete;
}

void Graph::getAnc(string term, vector<string>& anc){
  
  vector<string> par = str_term[term].getParent();
  anc.insert(anc.end(), par.begin(),par.end());
  for(string parent : str_term[term].getParent()){
    getAnc(parent,anc);
  }
}
string Graph::getRegulateTerm(string term){
  string rg = str_term[term].getRegulateTerm();
  if(rg.size()>0){
    return rg;
  }else{
    return term;
  }
  
}

set<string> Graph::getAncestors(string term) {
  vector<string> ancTerm=vector<string>();
  getAnc(term,ancTerm);
  set<string> s = set<string>(ancTerm.begin(),ancTerm.end());
  return s;
}
void Graph::getDesc(string term, vector<string>& dsc){
  vector<string> ch = str_term[term].getChildren();
  dsc.insert(dsc.end(), ch.begin(),ch.end());
  for(string child : str_term[term].getChildren()){
    getDesc(child,dsc);
  }
}
set<string> Graph::getDescendants(string term) {
  vector<string> dscTerm=vector<string>();
  getDesc(term,dscTerm);
  set<string> s = set<string>(dscTerm.begin(),dscTerm.end());
  return s;
}
void Graph::getDescLeaves(string term, vector<string>& dsc){
  vector<string> ch = str_term[term].getChildren();
  if(str_term[term].getChildren().size()>0){
    for(string child : str_term[term].getChildren()){
      getDescLeaves(child,dsc);
    }
  }
  else{
    dsc.push_back(term);
  }
}
void Graph::setChildrens(){
  map<string, Term>::iterator it;
  
  for ( it = str_term.begin(); it != str_term.end(); it++ )
  {
    string key =  it->first;
    vector<string> parents = it->second.getParent();
    for(string p:parents){
      str_term[p].setChildren(key);
      
    }
    vector<string> pofParents = it->second.getPOFParent();
    for(string p:pofParents){
      Term parent = str_term[p];
      parent.setPOFChildren(key);
    }
    
    
  }
}
set<string> Graph::getDescendantsLeaves(string term) {
  vector<string> dscTerm=vector<string>();
  getDescLeaves(term,dscTerm);
  set<string> s = set<string>(dscTerm.begin(),dscTerm.end());
  return s;
}

void Graph::computeInfoTerm()
{
  for(map<string,OntoInfo>::iterator it = topTerms.begin(); it != topTerms.end(); ++it){
    string sub = it->first;
    int flag=1;
    int level = 0;
    str_term[sub].set_Hscore(level);
    level++;
    vector<string> vec1 = vector<string>(str_term[sub].getChildren());
    vector<string> vec2 = vector<string>();
    while(flag==1){
      
      for(string element:vec1){
        str_term[element].set_Hscore(level);
        vector<string> ch = str_term[element].getChildren();
        vec2.insert(vec2.end(),ch.begin(),ch.end());
      }
      vec1.clear();
      if(vec2.size()>0){
        vec1.insert(vec1.end(),vec2.begin(),vec2.end());
        vec2.clear();
        level++;
      }else {
        flag=0;
        it->second.setMaxDepth(level);
      }
    }
    
    set<string> setDesc = getDescendants(sub);
    set<string> setLeaves = getDescendantsLeaves(sub);
    computeGOUniverseIC(sub);
    
    str_term[sub].setIC(0.);
    str_term[sub].setIC(0.);
    str_term[sub].setIC(0.);
    vector<long double> icsSec;
    vector<long double> icsZho;
    vector<long double> icsSan;
    for(string t : setDesc){
      long double probSeco = ((long double) getDescendants(t).size()+1.)/((long double)setDesc.size()+1.);
      long double probSanchez = ((long double) getDescendantsLeaves(t).size()/(long double)getAncestors(t).size()+1.)/((long double) setLeaves.size());
      long double icSeco = 1.-log((long double) getDescendants(t).size()+1.)/log((long double)setDesc.size()+1.);
      str_term[t].setIC(icSeco);
      icsSec.push_back(icSeco);
      long double icZhou=0.5*(icSeco)+(1.-0.5)*(log(str_term[t].getDepth())/log(it->second.getMaxDepth()));
      str_term[t].setIC(icZhou);
      icsZho.push_back(icZhou);
      long double icSanchez = -log(probSanchez);
      str_term[t].setIC(icSanchez);
      icsSan.push_back(icSanchez);
      
    }
    long double ICmaxSeco = *max_element(begin(icsSec), end(icsSec));
    it->second.setMaxIC(ICmaxSeco);
    long double ICmaxZhou = *max_element(begin(icsZho), end(icsZho));
    it->second.setMaxIC(ICmaxZhou);
    long double ICmaxSanchez = *max_element(begin(icsSan), end(icsSan));
    it->second.setMaxIC(ICmaxSanchez);
    
  }
}

void Graph::computeGOUniverseIC(string sub){
  vector<long double> icsMaz;
  str_term[sub].setIC(0.);
  str_term[sub].alphaBetaMazandu[0] =1.;
  str_term[sub].alphaBetaMazandu[1] =0.;
  for(string lt:getDescendantsLeaves(sub)){
    vector<long double> vecval = goUniverseIC(lt,sub);
    long double l10 = log(10);
    long double val= -log(vecval[0])-vecval[1]*l10;
    icsMaz.push_back(val);
  }
  long double ICmaxMazandu = *max_element(begin(icsMaz), end(icsMaz));
  topTerms[sub].setMaxIC(ICmaxMazandu);
  
}

vector<long double> Graph::goUniverseIC(string t,string top){
  long double alpha = 1.;
  long double beta = 0.;
  for(string par : str_term[t].getParent()) {
    if(par.compare(top)!=0){
      vector<long double> pereAlphaBeta = goUniverseIC(par,top);
      long double division =pereAlphaBeta[0]/(long double) str_term[par].getChildren().size();
      long double b = floor(log10(division));
      long double a =division/pow(10,b); 
      
      alpha = alpha*a;
      beta = beta + pereAlphaBeta[1] + b;
    }else{
      long double division =1./(long double) str_term[par].getChildren().size();
      long double b = floor(log10(division));
      long double a =division/pow(10,b); 
      
      alpha = alpha*a;
      beta = beta + b;
    }
    
  }
  str_term[t].alphaBetaMazandu[0] = alpha;
  str_term[t].alphaBetaMazandu[1] = beta;
  long double l10 = log(10);
  long double ic =-log(alpha)-beta*l10;
  if(!isnan(ic)){
    str_term[t].setIC(ic);
    return str_term[t].alphaBetaMazandu ;
  }else{
    vector<long double> res = vector<long double>(2);
    res.push_back(0);
    res.push_back(0);
    return res;
  }
  
  
}


void Graph::externalIC(){
  
  for(map<string,OntoInfo>::iterator it = topTerms.begin(); it != topTerms.end(); ++it){
    vector<double> maxExt;
    string sub = it->first;
    str_term[sub].setExternalIC(0.);
    str_term[sub].setAggregateIC(0.);
    int topsize = getAllGenes(sub).size();
    for(string d : getDescendants(sub)){
      set<string> g = getAllGenes(d);
      long double ic = -log((long double)g.size()/(long double)topsize);
      str_term[d].setExternalIC(ic);
      maxExt.push_back(ic);
    }
    topTerms[sub].setMaxExternalIC(*max_element(maxExt.begin(),maxExt.end()));
    for(string d : getDescendants(sub)){
      if(str_term[d].annotation.getGenome().size()>0){
        double aic = str_term[d].getSValueIC();
        for(string anc:getAncestors(d)){
          aic = aic +str_term[anc].getSValueIC();
        }
        str_term[d].setAggregateIC(aic);
      }
    }
  }
  
}


set<string> Graph::getAllGenes(string t){
  set<string> desc = getDescendants(t);
  set<string> genes = str_term[t].annotation.getGenome();
  for(string d : desc){
    set<string> gD = str_term[d].annotation.getGenome();
    genes.insert(gD.begin(),gD.end());
  }
  return genes;
}
}

namespace repspace{

/*
* REPRESENTATIVE CLASS
*/

class Representative
{
public:
  vector<string> getCombinedTerm();
  vector<string> getRepresentedTerms();
  Representative(vector<string> t, vector<string> rt);
  Representative();
  Representative(string t);
private:
  vector<string> combinedTerms;
  vector<string> representedTerms;
};

Representative::Representative(vector<string> t, vector<string> rt){
  combinedTerms = t;
  representedTerms=rt;
}
Representative::Representative(){
  
}
Representative::Representative(string t){
  combinedTerms.push_back(t);
  representedTerms.push_back(t);
  
}
vector<string> Representative::getCombinedTerm(){
  return combinedTerms;
}
vector<string> Representative::getRepresentedTerms(){
  return representedTerms;
}

/*
* ALGOREP CLASS
*/
class AlgoRep
{
public:
  int getIncompleteIC();
  string getOntologyString();
  //map<string,object> getMapping();
  string getMethodHCL();
  long double getTailMin();
  long double getSemSimBT();
  long double getTermCoverage();
  int getGeneSupport();
  vector<Representative> run( XPtr<ontologyF::Graph> go, vector<string> terminos, long double percIC_incomplete);
  AlgoRep();
  AlgoRep(int ic_inc,string subOnt,long double tailmin,long double simRepFilter,long double coverage, int geneSupport);
private:
  int ic_inc;
  string subOnt;
  string mhcl;
  long double tailmin;
  long double simRepFilter;
  long double termcoverage;
  int geneSupport;
  void ANDfunction(vector<bool> boolSource, vector<bool> &boolTarget);
  void ANDNOTfunction(vector<bool> boolSource, vector<bool> &boolTarget);
  void ORfunction(vector<bool> boolSource, vector<bool> &boolTarget);
  bool SUMfunction(vector<bool> boolS);
  Representative getRepresentative( XPtr<ontologyF::Graph> go, vector<string> terms);
  set<string> getManyRep(vector<string> terms,map<string,ontologyF::Term> &str_term , int ncombi);
  set<string> getOneRep(map<string,ontologyF::Term> str_term, set<string> termSubGraph, int termsize, map<string,vector<bool> >strBs);
  set<set<string> > doCombine(string can, vector<string> consideredC, int termsize,map<string,ontologyF::Term> str_term, int limit, map<string,vector<bool> >strBs);
  set<vector<string> > combination(vector<string> el, int k);
  string setCombinedTermInMap(set<string> cand, map<string,ontologyF::Term> &str_term, map<string,vector<bool> > &strBs);
};

AlgoRep::AlgoRep(){
  ic_inc = 0;
  subOnt="GO:0008150";
  tailmin=0.1;
  simRepFilter=0.5;
  termcoverage=1.;
  geneSupport=3;
}

AlgoRep::AlgoRep(int ic, string ont, long double tm, long double sRF, long double cov, int gS){
  ic_inc = ic;
  subOnt=ont;
  tailmin=tm;
  simRepFilter=sRF;
  termcoverage=cov;
  geneSupport=gS;
}

int AlgoRep::getIncompleteIC(){
  return ic_inc;
}
string AlgoRep::getOntologyString(){
  return subOnt;
}
string AlgoRep::getMethodHCL(){
  return mhcl;
}
long double AlgoRep::getTailMin(){
  return tailmin;
}
long double AlgoRep::getSemSimBT(){
  return simRepFilter;
}
long double AlgoRep::getTermCoverage(){
  return termcoverage;
}
int AlgoRep::getGeneSupport(){
  return geneSupport;
}

/*
* ALGORITHM
*/
vector<Representative> AlgoRep::run( XPtr<ontologyF::Graph> go, vector<string> terms, long double percIC_incomplete){
  vector<Representative> res;
  Representative r;
  if(terms.size()>1){
    r= this->getRepresentative(go, terms);
  } else{
    //cout<<go->str_term[terms.at(0)].getName()<<"\n";
    //cout<<go->str_term[terms.at(0)].getIC(ic_inc)<<"\n";
    r = Representative(terms.at(0));
  }
  if(r.getCombinedTerm().size()>0){
    set<string> genes;
    long double ic = 0;
    for(string t : r.getCombinedTerm()){
      // //cout<<go->str_term[t].annotation.getGenome().size()<<" "<<go->str_term[t].annotation.getGeneSet().size()<<" "<<go->str_term[t].getIC(0);
      set<string> g = go->str_term[t].annotation.getGeneSet();
      ic += go->str_term[t].getIC(ic_inc);
      genes.insert(g.begin(),g.end());
    }
    ic = ic/r.getCombinedTerm().size();
    if(ic>percIC_incomplete && genes.size()>geneSupport){
      res.push_back(r);
    }
  }
  return res;
  
}

Representative AlgoRep::getRepresentative( XPtr<ontologyF::Graph> go, vector<string> terms){
  long double tolerance = 0.7;
  map<string,ontologyF::Term> str_term = go->str_term;
  set<string> genesInCluster;
  for(string t : terms){
    set<string> gs =  go->str_term[t].annotation.getGeneSet();
    genesInCluster.insert(gs.begin(),gs.end());
  }
  vector<long double> vecICs;
  vector<string> strCandidate;
  
  for(string t:terms){
    int gsT = go->str_term[t].annotation.getGeneSet().size();
    
    if(gsT>=this->geneSupport){
      long double probGSannot = (long double) gsT/(long double) genesInCluster.size();
      if(probGSannot>=tolerance){
        strCandidate.push_back(t);
      }
    }
    
  }
  
  if(strCandidate.size()>0){
    for(int i=0;i<strCandidate.size();i++){
      set<string> r1 = go->getDescendants(strCandidate.at(i));
      for(int j=i+1;j<strCandidate.size();j++){
        set<string> r2 = go->getAncestors(strCandidate.at(j));
        if(r1.find(strCandidate.at(j))!=r1.end()){
          strCandidate.erase(strCandidate.begin()+i);
          i--;
          break;
        }else if(r2.find(strCandidate.at(i))!=r1.end()){
          strCandidate.erase(strCandidate.begin()+j);
          j--;
        }
      }
    }
  }
  vector<long double> ics;
  vector<string> candidate;
  if(strCandidate.size()==1){
    ontologyF::Term res = go->str_term[strCandidate.at(0)];
    ics.push_back(res.getIC(ic_inc));
    candidate.push_back(strCandidate.at(0));
  }else{
    
    
    
    int ncombi = terms.size()>10?((int) floor(sqrt(fabs((long double)terms.size()/10.-1.)))+2):1 ; // numero que indica el numero de combinaciones deseadas
    // cout<<ncombi<<"\n";
    ncombi = ncombi >6? 6:ncombi;
    set<string> repSet = getManyRep(terms, str_term,ncombi);
    
    
    for(string rep:repSet){
      candidate.push_back(rep);
      ics.push_back(str_term[rep].getIC(ic_inc));
    }
  }
  
  if(candidate.size()>0){
    
    vector<long double>::iterator it = max_element(ics.begin(),ics.end());
    string termC = candidate.at(it-ics.begin());
    
    if(str_term[termC].combinedTerm.size()>0){
      
      vector<string> ct = str_term[termC].combinedTerm;
      Representative r = Representative(ct,terms);
      
      return r;
    }else{
      
      vector<string> ct;
      ct.push_back(termC);
      Representative r = Representative(ct,terms);
      
      return r;
    }
  }else{
    
    return Representative();
  }
  
  
}

void AlgoRep::ORfunction(vector<bool> boolSource, vector<bool> &boolTarget){
  
  for(int i=0;i<boolSource.size();i++){
    boolTarget[i]= boolTarget[i] || boolSource[i];
  }
  
}
void AlgoRep::ANDfunction(vector<bool> boolSource, vector<bool> &boolTarget){
  
  for(int i=0;i<boolSource.size();i++){
    boolTarget[i]= boolTarget[i] && boolSource[i];
  }
  
}
void AlgoRep::ANDNOTfunction(vector<bool> boolSource, vector<bool> &boolTarget){
  
  for(int i=0;i<boolSource.size();i++){
    boolTarget[i]= boolTarget[i] && !boolSource[i];
  }
  
}

bool AlgoRep::SUMfunction(vector<bool> vec){
  bool res;
  for (bool n : vec)
    res = res|| n;
  
  return res;
}


set<string> AlgoRep::getManyRep(vector<string> terms,map<string,ontologyF::Term> &str_term , int ncombi){
  
  
  
  const size_t sT = terms.size();
  vector<bool> bs = vector<bool>(sT);
  deque<string> stack1;
  std::map<std::string,std::vector<bool> > strBs ;
  for(int i = 0; i<terms.size();i++){
    string t = terms.at(i);
    
    stack1.push_back(t);
    strBs.insert(pair<string,vector<bool> >(t,bs));
    strBs[t][i] = true;
  }
  
  set<string> termSubGraph;
  while(stack1.size()>0){
    
    //  //cout << (find(stack1.begin(),stack1.end(),"GO:0019060")!=stack1.end());
    string t = stack1.at(0);
    termSubGraph.insert(t);
    ontologyF::Term term = str_term[t];
    
    for(string p:term.getParent()){
      if(strBs.find(p)!=strBs.end()){
        vector<bool> bsPivote = vector<bool>(sT);
        ORfunction(strBs[t],bsPivote);
        ANDNOTfunction(strBs[p],bsPivote);
        if(SUMfunction(bsPivote)){
          
          ORfunction(strBs[t],strBs[p]);
          stack1.push_back(p);
        }
      }else{
        strBs.insert(pair<string,vector<bool> >(p,bs));
        ORfunction(strBs[t],strBs[p]);
        stack1.push_back(p);
      }
    }
    stack1.erase(stack1.begin());   
  }
  
  set<string> candidates = getOneRep(str_term,termSubGraph,terms.size(),strBs);
  
  
  
  
  int limit = 2;
  
  while(limit<=ncombi){
    set<set<string> > candidatesManyRep;
    for(string can:candidates){
      
      vector<string> consideredChildren;
      ;
      for(string d : str_term[can].getChildren()){
        if(termSubGraph.find(d)!=termSubGraph.end()){
          
          vector<bool> b =  strBs[d];
          int mycount = std::count(b.begin(),b.end(),true);
          long double comparison = (long double)mycount/(long double)terms.size();
          if(comparison>=tailmin){
            consideredChildren.push_back(d);
          }
        }
      }
      
      
      set<set<string> > dC = doCombine(can,consideredChildren,terms.size() ,str_term, limit,strBs);
      
      for(set<string> sc : dC){
        set<string> nsc;
        for(string s: sc){
          vector<bool> b = strBs[s];
          set<string> x = getOneRep(str_term,termSubGraph, std::count(s.begin(),s.end(),true),strBs);
          nsc.insert(x.begin(),x.end());
        }
        candidatesManyRep.insert(nsc);
      }
      
    }
    
    if(candidatesManyRep.size()>0){
      candidates.clear();
      vector<string> firstFilteredCandidates;
      vector<long double> ics;
      for(set<string> cand:candidatesManyRep){
        string t = setCombinedTermInMap(cand,str_term,strBs);  
        firstFilteredCandidates.push_back(t);
        ics.push_back(str_term[t].getIC(ic_inc));
      }
      long double maxIC = ics.at(std::distance(ics.begin(),max_element(ics.begin(),ics.end()))-1);
      for(int i=0;i<firstFilteredCandidates.size();i++){
        string ffc = firstFilteredCandidates.at(i);
        if(ics.at(i)==maxIC){
          candidates.insert(ffc);
        }
      }
    }
    limit++;
  }
  
  return candidates;
}

string AlgoRep::setCombinedTermInMap(set<string> cand, map<string,ontologyF::Term> &str_term, map<string,vector<bool> > &strBs){
  string id;
  ontologyF::Term t = ontologyF::Term();
  vector<bool> bs;
  long double seco = 0;
  long double zhou = 0;
  long double sanchez = 0;
  long double mazandu = 0;
  
  
  for(string c:cand){
    t.combinedTerm.push_back(c);
    seco+=str_term[c].getIC(1);
    mazandu+=str_term[c].getIC(0);
    zhou+=str_term[c].getIC(2);
    sanchez+=str_term[c].getIC(3);
    id+=c+"_";
    if(bs.size()>0){
      ORfunction(strBs[c],bs);
    }else{
      bs = vector<bool>(strBs[c].size());
    }
  }
  id = id.substr(0,id.size()-1);
  t.setId(id);
  t.setIC(mazandu/cand.size());
  t.setIC(seco/cand.size());
  t.setIC(zhou/cand.size());
  t.setIC(sanchez/cand.size());
  
  str_term.insert(pair<string,ontologyF::Term>(id,t));
  strBs.insert(pair<string,vector<bool> >(id,bs));
  return id; 
}


set<vector<string> > AlgoRep::combination(vector<string> el, int k){
  set<vector<string> > lint;
  int N = el.size();
  
  if(k>N){
    return lint;
  }
  vector<int> combination = vector<int>(k);
  int r=0;
  int index = 0;
  
  while(r>=0){
    if(index<=(N+(r-k))){
      combination[r] = index;
      if(r==k-1){
        vector<string> li;
        for(int c:combination){
          li.push_back(el.at(c));
        }
        lint.insert(li);
        index++;
      }else{
        index = combination[r]+1;
        r++;
      }
    }else{
      r--;
      if(r>0){
        index = combination[r]+1;
      }else{
        index = combination[0]+1;
      }
    }
  }
  
  return lint;
}

set<set<string> > AlgoRep::doCombine(string can, vector<string> consideredC, int termsize,map<string,ontologyF::Term> str_term, int limit, map<string,vector<bool> >strBs){
  set<vector<string> > combiLS = combination(consideredC,limit);
  set<set<string> > combiSS;
  
  for(vector<string> ns: combiLS){
    
    vector<bool> boolSet;
    int control =0;
    for(int i = 0;i<ns.size();i++){
      string t1 = ns.at(i);
      for(int j = i+1;j<ns.size();j++){
        string t2 = ns.at(j);
        vector<bool> inter = vector<bool>(strBs[t1].size());
        ORfunction(strBs[t1],inter);
        ANDfunction(strBs[t2],inter);
        
        vector<bool> unionS = vector<bool>(strBs[t1].size());
        ORfunction(strBs[t1],inter);
        ORfunction(strBs[t2],inter);
        
        long double jaccard = ((long double) std::count(inter.begin(),inter.end(),true)/(long double) std::count(unionS.begin(),unionS.end(),true));
        
        if(jaccard>simRepFilter){
          control++;
          break;
        }
        
      }
      if(control!=0){break;}
      else{
        ORfunction(strBs[t1],boolSet);
      }
    }
    if(control==0&&((long double)std::count(boolSet.begin(),boolSet.end(),true)/(long double)termsize>=termcoverage)){
      set<string> fs;
      fs.insert(ns.begin(),ns.end());
      combiSS.insert(fs);
    }
  }
  return combiSS;
}





set<string> AlgoRep::getOneRep(map<string,ontologyF::Term> str_term, set<string> termSubGraph, int termsize, map<string,vector<bool> >strBs){
  deque<string> stack1;
  stack1.push_back(subOnt);
  set<string> candidates;
  while(stack1.size()>0){
    string t = stack1.at(0);
    int counter= 0;
    for(string c : str_term[t].getChildren()){
      vector<bool> b =  strBs[c];
      int mycount = std::count(b.begin(),b.end(),true);
      long double comparison = (long double)mycount/(long double)termsize;
      if(comparison>=termcoverage){
        stack1.push_back(c);
        counter++;
        
      }
    }
    if(counter==0){
      candidates.insert(t);
    }
    stack1.erase(stack1.begin());
  }
  return candidates;
}



}

namespace creatingTree{

class Tree

{
public:
  Tree();
  map<string,vector<string> > createHierarchy( XPtr<ontologyF::Graph>  go, vector<string> terms, int ic);
private:
  map<string,vector<string> > getHierarchy(XPtr<ontologyF::Graph> go , vector<string> terms, int ic);
  string mica(XPtr<ontologyF::Graph> go, string t,vector<string> term,int ic);
  void ANDfunction(vector<bool> boolSource, vector<bool> &boolTarget);
  void ANDNOTfunction(vector<bool> boolSource, vector<bool> &boolTarget);
  void ORfunction(vector<bool> boolSource, vector<bool> &boolTarget);
  bool SUMfunction(vector<bool> boolS);
  
};

Tree::Tree(){
  
}

map<string,vector<string> > Tree::createHierarchy( XPtr<ontologyF::Graph> go, vector<string> terms, int ic) {
  
  map<string,vector<string> > hierarchy = getHierarchy(go, terms, ic);
  return hierarchy;	
  
}

map<string,vector<string> > Tree::getHierarchy( XPtr<ontologyF::Graph> go, vector<string> terms, int ic){
  deque<string> pile;
  set<string> stockage;
  map<string,set<string> > hierarchy;
  map<string,vector<bool> >strBs;
  map<string,string> mapPC;
  
  const size_t sT = terms.size();
  vector<bool> bs = vector<bool>(sT);
  
  for(int i = 0; i < terms.size();i++){
    string t = terms.at(i);
    set<string> ss;
    hierarchy.insert(pair<string,set<string> >(t,ss));
    pile.push_back(t);
    strBs.insert(pair<string,vector<bool> >(t,bs));
    strBs[t][i] = true;
    set<string> anc = go->getAncestors(t);
    stockage.insert(anc.begin(),anc.end());
  }
  
  for(string t: stockage){
    strBs.insert(pair<string,vector<bool> >(t,bs));
  }
  stockage.insert(terms.begin(),terms.end());
  while(pile.size()>0){
    string x = pile.at(0);
    if(go->str_term[x].getParent().size()>0) {
      string a = mica(go,x,terms,ic); // get the parent whit the best IC. 
      //   cout<<x<<" - "<<a<<"\n";
      mapPC.insert(pair<string,string>(x,a));
      ORfunction(strBs[x],strBs[a]);
      pile.push_back(a);
    }
    
    pile.erase(pile.begin()); 
  }
  deque<string> queue;
  queue.insert(queue.begin(),terms.begin(),terms.end());		
  
  while(queue.size()>0) {
    string t = queue.at(0);
    deque<string> stack; // Cargo una pila
    stack.push_back(t);
    while(stack.size()>0) {
      
      string mica = mapPC[stack.at(0)];
      
      if(mica.size()>0) {
        vector<bool> pivot = vector<bool>(sT);
        ORfunction(strBs[mica],pivot);
        ANDNOTfunction(strBs[t],pivot);
        bool condition = SUMfunction(pivot);
        vector<bool> pivot2 = vector<bool>(sT);
        ORfunction(strBs[go->str_term[t].getTopID()],pivot2);
        ANDNOTfunction(strBs[mica],pivot2);
        bool condition2 = SUMfunction(pivot2);
        
        //cout<<condition<<" "<<condition2<<"\n";
        
        if(!condition&&condition2) { //if condition1 is false (means equals) and if condition2 is true (means non equals)  
          stack.push_back(mica);
        }
        else{
          if(hierarchy.find(mica)!= hierarchy.end()) {
            hierarchy[mica].insert(t);
          }else { 
            set<string> ss;
            hierarchy.insert(pair<string,set<string> >(mica, ss));
            hierarchy[mica].insert(t);
            queue.push_back(mica);
            
            
          }
        }
        
      }
      stack.erase(stack.begin());
    }
    queue.erase(queue.begin()); 
  }
  
  map<string,vector<string> > res_hierarchy;
  for(map<string,set<string> >::iterator it = hierarchy.begin(); it != hierarchy.end(); ++it){
    string first = it->first;
    set<string> second = it->second;
    vector<string> secConvert;
    secConvert.insert(secConvert.end(), second.begin(), second.end());
    res_hierarchy.insert(pair<string,vector<string> >(first,secConvert));
  }
  return res_hierarchy;
  
}


void Tree::ORfunction(vector<bool> boolSource, vector<bool> &boolTarget){
  
  for(int i=0;i<boolSource.size();i++){
    boolTarget[i]= boolTarget[i] || boolSource[i];
  }
  
}
void Tree::ANDfunction(vector<bool> boolSource, vector<bool> &boolTarget){
  
  for(int i=0;i<boolSource.size();i++){
    boolTarget[i]= boolTarget[i] && boolSource[i];
  }
  
}
void Tree::ANDNOTfunction(vector<bool> boolSource, vector<bool> &boolTarget){
  
  for(int i=0;i<boolSource.size();i++){
    
    // cout<<boolTarget[i]<<" "<<boolSource[i]<<" "<<(boolTarget[i] && !boolSource[i])<<"\n";
    
    boolTarget[i]= boolTarget[i] && !boolSource[i];
  }
  
}

bool Tree::SUMfunction(vector<bool> vec){
  bool res=false;
  for (bool n : vec){
    res = res || n;
  }
  return res;
}


string Tree::mica(XPtr<ontologyF::Graph> go, string t,vector<string> term,int ic) {
  
  
  double icMax = 0;
  string parentICMax = go->str_term[t].getTopID();
  
  for(string a : go->str_term[t].getParent()){
    if(std::find(term.begin(), term.end(), a) != term.end()) {
      parentICMax = a; // Only when exist a element in the bag of term.
      break;
    }
    if(go->str_term[a].getIC(ic)>icMax) {
      icMax = go->str_term[a].getIC(ic);
      parentICMax = a;
    }
    
  }
  return parentICMax;
}



}

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//


//' Reading obo file
//' 
//' @param go_file obo file
//' @return Rcpp object (geneontology object)
// [[Rcpp::export]]
RcppExport SEXP reader(String go_file) {
  Rcout <<fixed << setprecision(14)<<"Charging the ontology\n";
  string line;
  ifstream myfile (go_file);
  string delimiter = ":";
  string token = "";
  int count = 0;
  int countTerms =0;
  ontologyF::Term term;
  ontologyF::Graph go = ontologyF::Graph();
  
  
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      if(line==""){
        token=line;
        if(countTerms>0){
          if(count>0 && term.isObsolete()!=1){
           
            go.str_term.insert(pair<string, ontologyF::Term>(term.getId(),term));
            //go.str_annot.insert(pair<string, ontologyF::Annotation>(term.getId(),ontologyF::Annotation()));
            if(term.getParent().size()==0){
              go.topTerms.insert(pair<string, ontologyF::OntoInfo>(term.getId(),ontologyF::OntoInfo()));
            }
          }
        }
        countTerms=0;
      }
      if(line=="[Term]"){
        countTerms++;
        token=line;
        term = ontologyF::Term();
        count++;
        
        
      }
      
      if(token=="[Term]") {
        
        string key = line.substr(0, line.find(delimiter));
        string value = line.substr(line.find(delimiter)+2,line.size());
        // cout << key <<"-:-"<<value<<"\n";
        term.setKeyValue(key,value);
        // cout << line << '\n';
      }
    }
    myfile.close();
  }
  //else cout << "Unable to open file"; 
  go.setChildrens();
  go.computeInfoTerm();
  
  
  ontologyF::Graph *ggo = new ontologyF::Graph(go.str_term,go.topTerms);
  //Graph *ggo = new Graph();
  Rcpp::XPtr<ontologyF::Graph> ptr(ggo,true);
  //Rcout<<"Done.";
  return ptr;
}



//' Function to clear the association of genes of a set with GO terms
//' 
//' @param go_rcppObj Rcpp object (geneontology object)
// [[Rcpp::export]]
void clearGeneSets(SEXP go_rcppObj) {
  XPtr<ontologyF::Graph> go(go_rcppObj);
  map<string,ontologyF::OntoInfo> mapping = go->topTerms;
  for(map<std::string,ontologyF::OntoInfo>::iterator it = mapping.begin(); it != mapping.end(); ++it){
    string sub = it->first;
    for(string t:go->getDescendants(sub)){
      go->str_term[t].annotation.clearGeneSet();
    }
  }
}

//' Function to add a gene into a list of GO terms
//' 
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param gene a gene
//' @param go_terms list of GO terms
// [[Rcpp::export]]
void addGeneSet(SEXP go_rcppObj, String gene, StringVector go_terms) {
  XPtr<ontologyF::Graph> go(go_rcppObj);
  for(int i=0;i<go_terms.size();i++){
    
    string t = Rcpp::as<string>(go_terms[i]);
    go->str_term[t].annotation.setOneGeneInGenSet(gene);
    for(string anc : go->getAncestors(t)){
      go->str_term[anc].annotation.setOneGeneInGenSet(gene);
    }
  }
  
}



//' Function to add a gene into a given GO term
//' 
//' @param g gene
//' @param s GO term
//' @param go_rcppObj Rcpp object (geneontology object)
// [[Rcpp::export]]
void setGeneInTerm(String g, String s,SEXP go_rcppObj) { // 
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  
  go-> str_term[s].annotation.setOneGeneInGenome(g);
  
  for(string anc : go->getAncestors(s)){
    go-> str_term[anc].annotation.setOneGeneInGenome(g);
  }
}

bool exist(SEXP go_rcppObj, string t){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  return go->str_term.find(t)!=go->str_term.end();
}

bool exist(ontologyF::Graph go, string t){
  
  return go.str_term.find(t)!=go.str_term.end();
}

//' Function to remove inappropriate annotations (by redundancy and by incompletness)
//' 
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_terms list of GO terms
//' @param ic_inc IC threshold value 
//' @param ic_author IC author
//' @return Vector of appropriate terms
// [[Rcpp::export]]
StringVector ReduceRedundancyAndIncompletness(SEXP go_rcppObj,StringVector go_terms, long double ic_inc,int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  StringVector noINC;
  //Rcout<<"0\n";
  for(int i=0;i<go_terms.size();i++){
 
    string t = Rcpp::as<string>( go_terms[i]);
    //Rcout<<t<<"\n";
    if(exist(go,t)!=0){
      if(go->str_term[t].getIC(ic_author)>ic_inc){
        noINC.push_back(t);
      }
    }
    //Rcout<<"f\n";
  }
//Rcout<<"1\n";
  for(int i=0;i<noINC.size();i++){
    string t1 = Rcpp::as<string>(noINC[i]);
    set<string> anc1  = go->getAncestors(t1);
    for(int j=i+1; j<noINC.size();j++){
      string t2 = Rcpp::as<string>(noINC[j]);
      set<string> anc2 = go->getAncestors(t2);
      if(anc1.find(t2) != anc1.end()) {
        noINC.erase(j);
        j--;
      }else if(anc2.find(t1) != anc2.end()){
        noINC.erase(i);
        i--;
        break;
      }
    }
  }
  //Rcout<<"2\n";
  return noINC;
  
}

//' Function to get the regulate term if exist
//'
//' @param go_term a GO term
//' @param go_rcppObj Rcpp object (geneontology object)
//' @return GO term regulating go_term
// [[Rcpp::export]]
String getRegulateTerm(String go_term,SEXP go_rcppObj) { // If there no Regulate term, the same term is returned. 
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  string reg = go->getRegulateTerm(go_term);
  
  //go-> str_term[reg].annotation.setOneGeneInGenome(g);
  
  return reg;
}

//' Function to get the top term from a given GO term (BP, MF or CC)
//'
//' @param go_term GO term
//' @param go_rcppObj Rcpp object (geneontology object)
//' @return Top term of go_term
// [[Rcpp::export]]
String getTopTerm(String go_term,SEXP go_rcppObj) { // If there no Regulate term, the same term is returned. 
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  string top = go->str_term[go_term].getTop();
  return top;
}

//' Function to compute the IC Resnik (using the chose GOA)
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' 
// [[Rcpp::export]]
void computeExternalIC(SEXP go_rcppObj){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  go->externalIC();
}

//' Function to get all IC values
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param ic_author IC author
//' @return a vector with all ICs (by corresponding ic_author) of the GO terms 
//' 
// [[Rcpp::export]]
DoubleVector getAllIC(SEXP go_rcppObj, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  DoubleVector ics;
  map<string,ontologyF::OntoInfo> mapping = go->topTerms;
  for(map<std::string,ontologyF::OntoInfo>::iterator it = mapping.begin(); it != mapping.end(); ++it){
    string sub = it->first;
    ics.push_back(0.); // IC corresponding to the top;
    for(string d:go->getDescendants(sub)){
      ics.push_back(go->str_term[d].getIC(ic_author));
    }
  }
  return ics;
}


//' Function to get the representative term list from a given cluster of similar GO terms
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param cluster_terms Cluster of similar terms
//' @param IC_incompleteThreshold IC threshold
//' @param ic_author IC author
//' @param subOnt Gene Ontology subontology
//' @param tailmin Minimal number of terms to consider a representative term
//' @param simRepFilter Similarity between two representative terms to combine together
//' @param coverage Term threshold. If value is 1 The result must cover all terms in the cluster
//' @param geneSupport gene coverage threshold
//' @return List of representative terms
// [[Rcpp::export]]
StringVector getRepresentative(SEXP go_rcppObj,StringVector cluster_terms, long double IC_incompleteThreshold, int ic_author,String subOnt,long double tailmin,long double simRepFilter,long double coverage, int geneSupport){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  repspace::AlgoRep ar = repspace::AlgoRep(ic_author,subOnt,tailmin,simRepFilter,coverage,geneSupport);
  vector<string> terms = Rcpp::as< vector<string> >(cluster_terms);
  //cout<<IC_incompleteThreshold<<endl;
  vector<repspace::Representative>  rep = ar.run(go,terms,IC_incompleteThreshold);
  
  StringVector results;
  for(repspace::Representative r:rep){
    for(string t: r.getCombinedTerm()){
      results.push_back(t);
    }
    
  }
  
  return results;

}


//' Function to compute a reduction step to remove inconsistent representative terms
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_Rep_terms list of representative terms
//' @return List of adequated representative terms
// [[Rcpp::export]]
StringVector ReduceRepTerms(SEXP go_rcppObj,StringVector go_Rep_terms){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  for(int i = 0; i<go_Rep_terms.size();i++) {
    string t1 = Rcpp::as<string>(go_Rep_terms[i]);
    set<string> anc1 = go->getAncestors(t1);
    
    for(int j= i+1; j<go_Rep_terms.size();j++) {
      string t2 = Rcpp::as<string>(go_Rep_terms[j]);
      set<string> anc2 = go->getAncestors(t2);
      
      if(anc1.find(t2) != anc1.end()) {
        go_Rep_terms.erase(j);
        j--;
      }else if(anc2.find(t1) != anc2.end()) {
        //						System.out.println(tj.toName() + " remove "+ti.toName());
        go_Rep_terms.erase(i);
        i--;
        break;
        
      }
    }
  }
  
  for(int i = 0; i<go_Rep_terms.size();i++) {
    string t1 = Rcpp::as<string>(go_Rep_terms[i]);
    vector<string> pOf1 = go->str_term[t1].getPOFParent();
    for(int j= i+1; j<go_Rep_terms.size();j++) {
      string t2 = Rcpp::as<string>(go_Rep_terms[j]);
      vector<string> pOf2 = go->str_term[t2].getPOFParent();
      
      if(find(pOf1.begin(),pOf1.end(),t2) != pOf1.end()) {
        go->str_term[t2].annotation.setGeneSet(go->str_term[t1].annotation.getGeneSet());
        if(go->str_term[t2].annotation.getGeneSet().size()>go->str_term[t1].annotation.getGeneSet().size()) {
          go_Rep_terms.erase(i);
          i--;
          break;
        }else {
          go_Rep_terms.erase(j);
          j--;
        }
      }else if(find(pOf2.begin(),pOf2.end(),t1) != pOf2.end()) {
        go->str_term[t2].annotation.setGeneSet(go->str_term[t1].annotation.getGeneSet());
        if(go->str_term[t2].annotation.getGeneSet().size()>go->str_term[t1].annotation.getGeneSet().size()) {
          go_Rep_terms.erase(i);
          i--;
          break;
        }else {
          go_Rep_terms.erase(j);
          j--;
        }
      }
      
    }
  }
  
  return go_Rep_terms;
}


//' Function to compute the set cover problem algorithm to get the most synthetic terms
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_Rep_terms List of representative terms
//' @return List of synthetic terms
// [[Rcpp::export]]
StringVector wSetCoverProblem(SEXP go_rcppObj, StringVector go_Rep_terms){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  map<string,double> mapT2localIC;
  set<string> setOfGenes;
  for(int j= 0; j<go_Rep_terms.size();j++) {
    string r = Rcpp::as<string>(go_Rep_terms[j]);
    double d = -log((double)go->str_term[r].annotation.getGeneSet().size()/(double)go->str_term[go->str_term[r].getTopID()].annotation.getGeneSet().size());
    mapT2localIC.insert(pair<string, double>(r,d));
    set<string> sett = go->str_term[r].annotation.getGeneSet();
    setOfGenes.insert(sett.begin(),sett.end());
  }
  
  
  
  vector<string> scpTerms;
  vector<string> essentials;
  vector<string> nonessential;
  set<string> finalGeneSet;
  for(int j= 0; j<go_Rep_terms.size();j++) {
    // cout<<rep.size()<<" ";
    string r = Rcpp::as<string>(go_Rep_terms[j]);
    vector<string> othersTermsTot;
    othersTermsTot.insert(othersTermsTot.end(),go_Rep_terms.begin(),go_Rep_terms.end());
    othersTermsTot.erase(othersTermsTot.begin()+j);
    // cout<<othersTermsTot.size()<<" \n";
    set<string> cover;
    for(int z= 0; z<othersTermsTot.size();z++) {
      string o = othersTermsTot[z];
      set<string> sett = go->str_term[o].annotation.getGeneSet();
      cover.insert(sett.begin(),sett.end());
    }
    
    if(cover.size()!=setOfGenes.size()) {
      essentials.push_back(r);
      scpTerms.push_back(r);
      set<string> ss = go->str_term[r].annotation.getGeneSet();
      finalGeneSet.insert(ss.begin(),ss.end());
    }else{
      nonessential.push_back(r);
    }
    
    
  }
  
  while(finalGeneSet.size()!=setOfGenes.size()){
    vector<double> li;
    
    for(string t : nonessential){
      set<string> genesT = go->str_term[t].annotation.getGeneSet();
      
      std::set<string> result;
      std::set_difference(genesT.begin(), genesT.end(), finalGeneSet.begin(), finalGeneSet.end(),
                          std::inserter(result, result.end()));
      
      if(result.size()==0) {
        li.push_back(0.);
      }else {
        double d = (double)result.size()*(go->str_term[t].getExternalIC()/mapT2localIC[t]);
        li.push_back(d);
        
      }	
      
    }
    
    
    int pos = distance(li.begin(), max_element (li.begin(),li.end()));
    string choosen = nonessential.at(pos);
    scpTerms.push_back(choosen);
    nonessential.erase(nonessential.begin()+pos);
    set<string> ss = go->str_term[choosen].annotation.getGeneSet();
    finalGeneSet.insert(ss.begin(),ss.end());
    
    
    
    
  }
  
  
  
  StringVector sv;
  for(int z= 0; z<scpTerms.size();z++) {
    sv.push_back(scpTerms.at(z));
  }
  
  
  
  
  return sv;
  
}

//' Function to check if a GO term is into the GO structure
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_term GO term 
//' @return boolean
// [[Rcpp::export]]
bool exists(SEXP go_rcppObj, String go_term){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  return(go->str_term.find(go_term)!=go->str_term.end());
}
    
//' Function to compute the semantic similarity  -- Nunivers
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_term1 GO term 1
//' @param go_term2 GO term 2
//' @param ic_author IC author
//' @return semantic similarity score (double)                      
// [[Rcpp::export]]
double NUniversFunction(SEXP go_rcppObj, String go_term1, String go_term2, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  
  
  
  set<string> setAnc1 = go->getAncestors(go_term1);
  setAnc1.insert(go_term1);
  set<string> setAnc2 = go->getAncestors(go_term2);
  setAnc2.insert(go_term2);
  vector<string> inter;
  std::set_intersection(setAnc1.begin(), setAnc1.end(),
                        setAnc2.begin(), setAnc2.end(),
                        std::back_inserter(inter));
  
  vector<long double> ics;
  if(inter.size()>0){
    for(string anc: inter){
      ics.push_back(go->str_term[anc].getIC(ic_author));
    }
    
    long double icMax = *max_element(ics.begin(),ics.end());
    long double tICMax= max(go->str_term[go_term1].getIC(ic_author),go->str_term[go_term2].getIC(ic_author));
    return icMax/tICMax;
  }else{
    return 0.;
  }
  
}

//' Function to compute the semantic similarity  -- Resnik
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_term1 GO term 1
//' @param go_term2 GO term 2
//' @param ic_author IC author
//' @return semantic similarity score (double)
// [[Rcpp::export]]
double ResnikFunction(SEXP go_rcppObj, String go_term1, String go_term2, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  
  
  
  set<string> setAnc1 = go->getAncestors(go_term1);
  setAnc1.insert(go_term1);
  set<string> setAnc2 = go->getAncestors(go_term2);
  setAnc2.insert(go_term2);
  vector<string> inter;
  std::set_intersection(setAnc1.begin(), setAnc1.end(),
                        setAnc2.begin(), setAnc2.end(),
                        std::back_inserter(inter));
  
  vector<long double> ics;
  if(inter.size()>0){
    for(string anc: inter){
      ics.push_back(go->str_term[anc].getExternalIC());
    }
    
    long double icMax = *max_element(ics.begin(),ics.end());
   
    return icMax/go->topTerms[go->str_term[go_term1].getTopID()].getMaxExternalIC();
  }else{
    return 0.;
  }
  
}

//' Function to compute the semantic similarity -- Lin
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_term1 GO term 1
//' @param go_term2 GO term 2
//' @param ic_author IC author
//' @return semantic similarity score (double)
// [[Rcpp::export]]
double LinFunction(SEXP go_rcppObj, String go_term1, String go_term2, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  
  
  
  set<string> setAnc1 = go->getAncestors(go_term1);
  setAnc1.insert(go_term1);
  set<string> setAnc2 = go->getAncestors(go_term2);
  setAnc2.insert(go_term2);
  vector<string> inter;
  std::set_intersection(setAnc1.begin(), setAnc1.end(),
                        setAnc2.begin(), setAnc2.end(),
                        std::back_inserter(inter));
  
  vector<long double> ics;
  if(inter.size()>0){
    for(string anc: inter){
      ics.push_back(go->str_term[anc].getExternalIC());
    }
    
    long double icMax = *max_element(ics.begin(),ics.end());
    
    return 2*icMax/(go->str_term[go_term1].getIC(ic_author)+go->str_term[go_term2].getIC(ic_author));
  }else{
    return 0.;
  }
  
}

//' Function to compute the semantic similarity -- AIC
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_term1 GO term 1
//' @param go_term2 GO term 2
//' @param ic_author IC value provided by an author
//' @return semantic similarity score (double)
// [[Rcpp::export]]
double AICFunction(SEXP go_rcppObj, String go_term1, String go_term2, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  set<string> setAnc1 = go->getAncestors(go_term1);
  setAnc1.insert(go_term1);
  set<string> setAnc2 = go->getAncestors(go_term2);
  setAnc2.insert(go_term2);
  vector<string> inter;
  std::set_intersection(setAnc1.begin(), setAnc1.end(),
                        setAnc2.begin(), setAnc2.end(),
                        std::back_inserter(inter));
  
  double aicics = 0.;
  if(inter.size()>0){
    for(string anc: inter){
      aicics = aicics +go->str_term[anc].getSValueIC();
    }
    
    
    return 2*aicics/(go->str_term[go_term1].getAggregateIC()+go->str_term[go_term2].getAggregateIC());
  }else{
    return 0.;
  }
  
}

//' Function to compute the semantic similarity -- DF
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param go_term1 GO term 1
//' @param go_term2 GO term 2
//' @param ic_author IC value provided by an author
//' @return semantic similarity score (double)
// [[Rcpp::export]]
double DFFunction(SEXP go_rcppObj, String go_term1, String go_term2, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  
  
  
  
  set<string> setAnc1 = go->getAncestors(go_term1);

  set<string> setAnc2 = go->getAncestors(go_term2);

  vector<string> inter;
  std::set_intersection(setAnc1.begin(), setAnc1.end(),
                        setAnc2.begin(), setAnc2.end(),
                        std::back_inserter(inter));
  
  set<string> uni;
  uni.insert(setAnc1.begin(),setAnc1.end());
  uni.insert(setAnc2.begin(),setAnc2.end());

    return 1.- ((double)uni.size()-inter.size())/((double)uni.size());
 
  
}



List creaTree(string term, map<string,vector<string> >hierarchy, map<string,set<string> >mapTerm2genes){
  
  List lFinal;
  
  if(hierarchy.at(term).size()>0){
    int nt = hierarchy[term].size();
    int ng = mapTerm2genes[term].size();
    List lt(nt+ng);
    for(int i=0; i<nt ;i++){
      string t = hierarchy[term].at(i);
      List l = creaTree(t,hierarchy,mapTerm2genes);
      lt[i] = l;
    }
    int pos = nt;
    for(string g:mapTerm2genes[term]){
      List l = List::create(_["id"]=g,_["size"]=1);
      lt[pos]=l;
      pos++;
    }
    lFinal = List::create(_["id"]=term, _["children"]=lt);
  }else{
    int ng = mapTerm2genes[term].size();
    List lt(ng);
    int pos = 0;
    for(string g:mapTerm2genes[term]){
      List l = List::create(_["id"]=g,_["size"]=1);
      lt[pos]=l;
      pos++;
    }
    lFinal = List::create(_["id"]=term, _["children"]=lt);
  }
  
  return lFinal;
}

//' Function to generate a JSON object with the GSAn result
//'
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param gene gene vector
//' @param scp synthetic GO terms vector
//' @param repTermsReduced representative terms reduced vector
//' @param ic_author IC author
//' @return List object including the JSON
// [[Rcpp::export]]
List creatingJSON(SEXP go_rcppObj,StringVector gene, StringVector scp, StringVector repTermsReduced, int ic_author){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  vector<string> geneset  = Rcpp::as<vector<string> >(gene);
  vector<string> setCP = Rcpp::as<vector<string> >(scp);
  //vector<string> representative = Rcpp::as<vector<string> >(repTerms);
  vector<string> repReduced = Rcpp::as<vector<string> >(repTermsReduced);
  creatingTree::Tree tree;
  map<string,vector<string> > hierarchy =  tree.createHierarchy(go,repReduced,ic_author);
  
  map<string,set<string> > mapTerm2genes;
  for(map<string,vector<string> >::iterator it = hierarchy.begin(); it != hierarchy.end(); ++it)
  {
    string t = it->first;
    mapTerm2genes.insert(pair<string,set<string> >(t, go->str_term[t].annotation.getGeneSet()));
  }
  
  List lf(5);
  if(hierarchy.size()>0){
    vector<string> subOnt;
    for(map<string,ontologyF::OntoInfo>::iterator it = go->topTerms.begin(); it != go->topTerms.end(); ++it){
      string ontology = it->first;
      if(hierarchy.find(ontology)!=hierarchy.end()){
        subOnt.push_back(ontology);
        vector<string> line1;
        line1.push_back(ontology);
        vector<string> line2;
        bool condition = false;
        while(condition==false){
          for(string t:line1){
            
            for(string t_ch:hierarchy[t]){
              set<string> result;
              std::set_difference(mapTerm2genes[t].begin(), mapTerm2genes[t].end(),
                                  mapTerm2genes[t_ch].begin(), mapTerm2genes[t_ch].end(),
                                  std::inserter(result, result.end()));
              //mapTerm2genes.insert(pair<string,set<string> >(t,result));
              mapTerm2genes[t] = result;
              line2.push_back(t_ch);
            }
          }
          if(line2.size()>0){
            line1.clear();
            line1.insert(line1.end(),line2.begin(),line2.end());
            line2.clear();
          }else{
            condition=true;
          }
        } 
        
      }
    }
    List listTree;
    List listDic;
    if(subOnt.size()>1){   
      
      for(map<string,vector<string> >::iterator it = hierarchy.begin(); it != hierarchy.end(); ++it){
        string t = it->first;
        StringVector sv;
        for(string g:mapTerm2genes[t]){
          sv.push_back(g);
        }
        List l = List::create(_["opacity"]=1., _["name"]=go->str_term[t].getName(),
                              _["geneSet"]=sv,_["onto"]=go->str_term[t].getTop(),
                              _["IC"]=go->str_term[t].getIC(ic_author));
        listDic[t]=l;
        
        
      }
      
      lf["terms"] = listDic;
      StringVector sv;
      List l = List::create(_["opacity"]=1., _["name"]="Gene Ontology",
                            _["geneSet"]=sv,
                            _["IC"]=0.);
      listDic["GO"]=l;
      List subT(subOnt.size());
      for(int i=0;i<subOnt.size();i++){
        subT[i]=creaTree(subOnt.at(i),hierarchy,mapTerm2genes);
      }
      listTree = List::create(_["id"]="GO",_["children"]=subT);
    } else{
      for(map<string,vector<string> >::iterator it = hierarchy.begin(); it != hierarchy.end(); ++it){
        string t = it->first;
        StringVector sv;
        for(string g:mapTerm2genes[t]){
          sv.push_back(g);
        }
        List l = List::create(_["opacity"]=1., _["name"]=go->str_term[t].getName(),
                              _["geneSet"]=sv,_["onto"]=go->str_term[t].getTop(),
                              _["IC"]=go->str_term[t].getIC(ic_author)); 
        //Rcout<<t<<"\n";
        listDic[t]=l;
        
        
      }
      lf["terms"] = listDic;
      listTree = creaTree(subOnt.at(0),hierarchy,mapTerm2genes);
    }
    
    lf["tree"] = listTree;
   
    lf["scp"] = scp;
    lf["representatives"] = repReduced;
    lf["GeneSet"] = gene;
    
  }
  return lf;
}

//' Function to set the IC distribution of GO structure
//' @param go_rcppObj Rcpp object (geneontology object)
//' @param ic IC value
// [[Rcpp::export]]
void setICDistribution(SEXP go_rcppObj, double ic){
  XPtr<ontologyF::Graph> go(go_rcppObj);
  go->setIC_incomplete(ic);
}
//' Function to get the IC distribution of GO structure
//' @param go_rcppObj Rcpp object (geneontology object)
//' @return double value
// [[Rcpp::export]]
double getICDistribution(SEXP go_rcppObj){
  XPtr<ontologyF::Graph> go(go_rcppObj);
 return go->getICIncomplete();
}



//filepath <- "../gsan/src/main/resources/static/AssociationTAB/goa_human.gaf"
//gaf<- flavin::read_gaf(filepath, filter.evidence = c("ND"), filter.NOT = T)
// Hace falta crear una lista de genes incluyendo GO, ONT, EVIDENCE
//' Creation of Annotation list 
//' @param df Annotation Table extracted by read_gaf
//' @param go_terms GO term vector
//' @param go_rcppObj Rcpp object (geneontology object)
//' @return List of geneontology annotation for each gene
// [[Rcpp::export]]
Rcpp::List createListAnnotation(Rcpp::DataFrame df, StringVector go_terms, SEXP go_rcppObj) {
  Rcout<<"Reading the GAF file - that can take some time (over 1 min.)\n";
  XPtr<ontologyF::Graph> go(go_rcppObj);
  StringVector genes= df.at(0);
  StringVector terms = df.at(1);
 // StringVector evidence = df.at(6);
  List list = List::create();
  set<string> elementInside = set<string>();
  map<string,set<string> > mapping = map<string,set<string> >();
  List goList = List::create();
  
  for(int i=0;i<go_terms.size();i++){
    
    string t = Rcpp::as<string>(go_terms[i]);
 
    if(exist(go,t)!=0){
    List c= List::create();
    c["GOID"] = getRegulateTerm(go_terms[i],go_rcppObj);
    c["Ontology"] = getTopTerm(go_terms[i],go_rcppObj);
    goList[t] = c;
    }
  }
 
  int count = 0;
  for(int i=0;i<terms.size();i++){
    string t = Rcpp::as<string>(terms[i]);
    if(exist(go,t)!=0){
    string g = Rcpp::as<string>(genes[i]);
 //   string ev = Rcpp::as<string>(evidence[i]);
   
    if(elementInside.find(g)==elementInside.end()){
      count = count+1;
      elementInside.insert(g);
     // list[g] = List::create();
      mapping.insert(pair<string,set<string> >(g,set<string>()));
    }
    mapping.at(g).insert(t);
   // List elGO = goList[t];
   // elGO["Evidence"] =ev;
  //  List goL = list[g];
  //  string e = elGO["GOID"]; 
  //  goL[e] = elGO; 
  //  list[g] = goL;
    
    
    go-> str_term[t].annotation.setOneGeneInGenome(g);
    for(string anc : go->getAncestors(t)){
      go-> str_term[anc].annotation.setOneGeneInGenome(g);
    }
  }
  }
  
  
  for(auto t : mapping){
    string g = t.first;
    List gol = List::create();
    for(string goT : t.second){
      List elGO = goList[goT];
       string e = elGO["GOID"];  
       gol[e] = elGO;
    }
    list[g] = gol;
    
  }
  Rcout<<count<<" genes in the GAF file\n";
  return(list);
}

