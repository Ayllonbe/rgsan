# rGSAn, a R package dedicated to the Gene Set Annotation
### -- UNSTABLE Version --


## Install package

```
# Install the latest version of this package by entering the following in R:
if(!requireNamespace("devtools", quietly = TRUE)){
  install.packages("devtools")}
library(devtools)
install_bitbucket("ayllonbe/rgsan")
```

## Example to run:

```
library(rGSAn)
# Download the org.Hs.eg.db R package by bioconductor
library(org.Hs.eg.db)
geneSet <- rGSAnTestEntrezGenes() # gene set exemple
result <- rGSAnComplete(geneSet,orgAnnot = org.Hs.egGO)
rGSAnResView(result)
```


